<?php

namespace Drupal\kuula_embed\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'kuula_field' field type.
 *
 * @FieldType(
 *   id = "kuula_field",
 *   label = @Translation("Kuula Embed"),
 *   description = @Translation("Embed Panorama model in pages."),
 *   category = @Translation("Media"),
 *   default_widget = "kuula_widget",
 *   default_formatter = "kuula_format"
 * )
 */
class KuulaEmbedItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'use_css' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'not null' => FALSE,
        ],
        'use_css' => [
          'description' => t("Use css to set size"),
          'type' => 'int',
          'unsigned' => TRUE,
          'default' => 0,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Target URL'));

    $properties['use_css'] = DataDefinition::create('boolean')
      ->setLabel(t('Select'))
      ->setDescription(t("Use css to set size"));

    return $properties;
  }

}
