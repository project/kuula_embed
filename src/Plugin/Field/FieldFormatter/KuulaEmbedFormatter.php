<?php

namespace Drupal\kuula_embed\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'kuula_field' formatter.
 *
 * @FieldFormatter(
 *   id = "kuula_format",
 *   label = @Translation("Kuula Embed"),
 *   field_types = {
 *     "kuula_field"
 *   }
 * )
 */
class KuulaEmbedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();
    $summary[] = t('Displays the Panorama Model from Kuula.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = [
        '#type' => 'inline_template',
        '#template' => '<iframe {{ style_attributes }} class="{{ ku_embed }}" frameborder="0" allow="xr-spatial-tracking; gyroscope; accelerometer" allowfullscreen scrolling="no" src="{{ url }}"></iframe>',
        '#context' => [
          'url' => $item->value,
          'ku_embed' => $item->use_css ? 'ku_embed' : '',
          'style_attributes' => $item->use_css ? '' : 'width="100%" height="640"',
        ],
      ];
    }

    return $element;
  }

}
