<?php

namespace Drupal\kuula_embed\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'kuula_field' field widget.
 *
 * @FieldWidget(
 *   id = "kuula_widget",
 *   label = @Translation("Kuula Embed"),
 *   field_types = {"kuula_field"},
 * )
 */
class KuulaEmbedWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'use_css' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = [
      '#type' => 'url',
      '#default_value' => $items[$delta]->value,
      '#title' => $this->t('Image URL'),
      '#description' => $this->t('Image URL'),
    ];
    $element += [
      'use_css' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Use css to set Width & Height'),
        '#default_value' => $items[$delta]->use_css,
        '#description' => $this->t('This will remove inline width & Height'),
      ],
    ];
    return $element;
  }

}
